//
// Created by Филипп Исполатов on 2019-11-09.
//

#ifndef APPLICATIONSERVER_TCPSERVER_H
#define APPLICATIONSERVER_TCPSERVER_H

#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <iostream>

#include "tcpServConnection.h"
#include "KeyManager.h"

using namespace boost::asio;

class TcpServer {
public:
    TcpServer(boost::asio::ip::address address, unsigned short port);
    void run();

protected:
    void accept();

    ip::tcp::endpoint endpoint;

    io_service service;

    ip::tcp::acceptor acceptor;

    ip::tcp::socket socket;

    KeyManager *manager;
};

#endif //APPLICATIONSERVER_TCPSERVER_H
