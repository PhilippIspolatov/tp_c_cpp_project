//
// Created by Филипп Исполатов on 19/11/2019.
//

#include <boost/enable_shared_from_this.hpp>
#include <boost/asio/completion_condition.hpp>
#include <boost/shared_ptr.hpp>
#include <iostream>
#include <models.h>

#include "tcpServConnection.h"

TcpServerConnection::TcpServerConnection(ip::tcp::socket socket, KeyManager *manager) : socket(std::move(socket)), manager(manager) {}

void TcpServerConnection::start() {
    doRead();
}

void TcpServerConnection::doRead() {
    auto self = shared_from_this();

    async_read(socket, readBuffer, boost::asio::transfer_at_least(3),
               [self](boost::system::error_code err, size_t bytes_transfered) {
                if (!err)
                   self->onRead();
               });
}

void TcpServerConnection::onRead() {
    std::istream is(&readBuffer);
    std::string s;
    is >> s;
    std::cout << s << std::endl;

    std::ostream os(&writeBuffer);

    msgpack::object_handle oh = msgpack::unpack(s.c_str(), s.size());
    msgpack::object deserialized = oh.get();

    std::cout << "Deserialized: " << deserialized << std::endl;

    auto query = deserialized.as<KeyQuery>();

    std::cout << query.username << std::endl;

    std::string result = manager->generateKey(query);

    auto response = KeyResponse();
    response.hash = result;
    std::cout << "Hash: " << response.hash << std::endl;
    msgpack::pack(os, response);
//    os << std::rand() % 5;
    doWrite();
}

void TcpServerConnection::doWrite() {
    auto self = shared_from_this();
    async_write(socket, writeBuffer, [self](boost::system::error_code err, size_t bytes) {
        if (!err)
        self->onWrite();
    });
}


void TcpServerConnection::onWrite() {

}
