//
// Created by Филипп Исполатов on 19/11/2019.
//

#include <boost/asio/ip/address.hpp>
#include <boost/asio.hpp>
#include <string>

#include "tcpServer.h"

int main(int argc, char* argv[]) {

    auto const address = boost::asio::ip::make_address("127.0.0.1");
    unsigned short port = static_cast<unsigned short>(5000);

    TcpServer *server = new TcpServer(address, port);
    server->run();

    return 0;
}
