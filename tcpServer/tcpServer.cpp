//
// Created by Филипп Исполатов on 19/11/2019.
//
#include "tcpServer.h"

using namespace boost::asio;

TcpServer::TcpServer(boost::asio::ip::address address, unsigned short port) :
    endpoint(address, port),
    service(1),
    acceptor(service, endpoint),
    socket(service) {
    boost::system::error_code err;
    acceptor.set_option(socket_base::reuse_address(true), err);
    if (err) {
        std::cerr << "err2:" << err.message() << std::endl;
    }
    acceptor.listen(socket_base::max_listen_connections, err);
    if (err) {
        std::cerr << "err3:" << err.message() << std::endl;
    }

    manager = new KeyManager();
}


void TcpServer::accept() {
    acceptor.async_accept(socket, [&](boost::system::error_code err) {
        if (!err) {
            std::cout << "New connection" << std::endl;
            boost::make_shared<TcpServerConnection>(std::move(socket), manager)->start();
        }
        accept();
    });
}

void TcpServer::run() {
    accept();

    std::vector<std::thread> threads;
    for (int i = 0; i < 4; i++) {
        threads.push_back(std::thread([this](){
            service.run();
        }));
    }

    for (auto& thread : threads)  {
        thread.join();
    }

}