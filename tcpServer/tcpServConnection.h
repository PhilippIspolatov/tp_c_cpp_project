//
// Created by Филипп Исполатов on 2019-11-09.
//

#ifndef APPLICATIONSERVER_TCPSERVCONNECTION_H
#define APPLICATIONSERVER_TCPSERVCONNECTION_H

#include <boost/asio.hpp>
#include <boost/enable_shared_from_this.hpp>

#include <KeyManager.h>

using namespace boost::asio;


class TcpServerConnection : public boost::enable_shared_from_this<TcpServerConnection> {
public:
    explicit TcpServerConnection(boost::asio::ip::tcp::socket socket, KeyManager *manager);

    ~TcpServerConnection() = default;

    void start();

protected:
    void doRead();
    void onRead();
    void doWrite();
    void onWrite();

    boost::asio::ip::tcp::socket socket;
    boost::asio::streambuf readBuffer;
    boost::asio::streambuf writeBuffer;

    KeyManager *manager;

};

#endif //APPLICATIONSERVER_TCPSERVCONNECTION_H
