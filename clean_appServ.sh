cd ./tcpClient
rm -rf cmake-build-debug
echo "TCP Client build was deleted ..."

cd ../database
rm -rf cmake-build-debug
echo "Database build was deleted ..."

cd ../httpServer
rm -rf cmake-build-debug
echo "HTTP Server build was deleted ..."
