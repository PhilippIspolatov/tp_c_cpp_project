#include <iostream>

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include "../applicationServer/httpServer/router.h"
#include "../applicationServer/httpServer/httpServConnection.h"
#include "../applicationServer/types.h"
#include "../applicationServer/httpServer/httpServer.h"
#include "../applicationServer/managers/profileManager.h"
#include "../applicationServer/managers/keyManager.h"
#include "../applicationServer/managers/cacheManager.h"

using ::testing::AtLeast;
using ::testing::DoAll;
using ::testing::Return;
using ::testing::SetArgReferee;

class MockHttpServerConnection : public HttpServerConnection {
public:
    explicit MockHttpServerConnection(ip::tcp::socket socket) : HttpServerConnection(std::move(socket), nullptr) {};
    MOCK_METHOD(bool, isStart, (), (override));
    MOCK_METHOD(void, start, (), (override));
    MOCK_METHOD(void, stop, (), (override));
    MOCK_METHOD(void, doRead, (), (override));
    MOCK_METHOD(void, onRead, (), (override));
    MOCK_METHOD(void, doWrite, (), (override));
    MOCK_METHOD(void, onWrite, (), (override));
};

//class MockRouter : public Router {
//public:
//    MOCK_METHOD(responseType, execute, (requestType request));
//};

class MockProfileManager : public ProfileManager {
public:
    MockProfileManager() : ProfileManager(nullptr, nullptr) {}
    MOCK_METHOD(responseType, registrate, (map request), ());
    MOCK_METHOD(bool, userExist, (map request));
};

class MockKeyManager : public KeyManager {
public:
    MockKeyManager() : KeyManager(nullptr, nullptr, nullptr, nullptr) {}
    MOCK_METHOD(responseType, redirect, (map request));
    MOCK_METHOD(responseType, getInfo, (map request));
    MOCK_METHOD(responseType, genKey, (map request));
};

class MockUserInfoSerializer : public UserInfoSerializer {
public:
    MockUserInfoSerializer() = default;
    MOCK_METHOD(std::string, Serialize, (), (override));
    MOCK_METHOD(map, deSerialize, (requestType request), (override));
};

TEST(ServerTest, Serialize) {
    MockUserInfoSerializer serializer;

    EXPECT_CALL(serializer, Serialize()).Times(testing::AtLeast(1));

    ProfileManager profileManager(nullptr, &serializer);
    map request;
    profileManager.regisrate(request);
}


TEST(ServerTest, Redirect) {
    MockKeyManager keyManager;
    map request;

    EXPECT_CALL(keyManager, redirect(request)).Times(testing::AtLeast(1));

    Router router(nullptr, &keyManager, nullptr);
    http::request<http::string_body> req;
    req.target("/");
    router.execute(req);
}

TEST(ServerTest, GetInfo) {
    MockKeyManager keyManager;
    map request;

    EXPECT_CALL(keyManager, getInfo(request)).Times(testing::AtLeast(1));

    Router router(nullptr, &keyManager, nullptr);
    http::request<http::string_body> req;
    req.target("/history");
    router.execute(req);
}

TEST(ServerTest, GenKey) {
    MockKeyManager keyManager;
    map request;

    EXPECT_CALL(keyManager, genKey(request)).Times(testing::AtLeast(1));

    Router router(nullptr, &keyManager, nullptr);
    http::request<http::string_body> req;
    req.target("/generate");
    router.execute(req);
}

TEST(ServerTest, Registrate) {
    MockProfileManager profileManager;
    map request;

    EXPECT_CALL(profileManager, registrate(request)).Times(testing::AtLeast(1));

    Router router(&profileManager, nullptr, nullptr);
    http::request<http::string_body> req;
    req.target("/registrate");
    router.execute(req);
}

TEST(ServerTest, UserExist) {
    MockProfileManager profileManager;
    std::map<std::string, std::string> request;

    EXPECT_CALL(profileManager, userExist(request)).Times(testing::AtLeast(1));

    Router router(&profileManager, nullptr, nullptr);
    http::request<http::string_body> req;
    req.target("/generate");
    router.execute(req);
}



TEST(ServerTest, CanCreateConnection) {
    io_context context(1);
    ip::tcp::socket socket(context);
    MockHttpServerConnection connection(std::move(socket));

    EXPECT_CALL(connection, isStart()).Times(testing::AtLeast(1));
    EXPECT_CALL(connection, start()).Times(testing::AtLeast(1));
    EXPECT_CALL(connection, stop()).Times(testing::AtLeast(1));
    EXPECT_CALL(connection, doRead()).Times(testing::AtLeast(1));
    EXPECT_CALL(connection, onRead()).Times(testing::AtLeast(1));
    EXPECT_CALL(connection, doWrite()).Times(testing::AtLeast(1));
    EXPECT_CALL(connection, onWrite()).Times(testing::AtLeast(1));

    boost::asio::ip::address address;
    unsigned short port = 3000;
    Server server(address, port);
    server.run();
}

//TEST(ServerTest, Routing) {
//    MockRouter mockRouter;
//    http::request<http::string_body> req;
//    EXPECT_CALL(mockRouter, execute(req)).Times(testing::AtLeast(1));
//
//    io_context context(1);
//    ip::tcp::socket socket(context);
//    HttpServerConnection httpServerConnection(std::move(socket), nullptr);
//    httpServerConnection.start();
//}



int main(int argc, char** argv) {
    ::testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}