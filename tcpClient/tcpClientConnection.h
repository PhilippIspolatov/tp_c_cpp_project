//
// Created by Филипп Исполатов on 2019-11-09.
//

#ifndef APPLICATIONSERVER_TCPCLIENTCONNECTION_H
#define APPLICATIONSERVER_TCPCLIENTCONNECTION_H

#include <istream>
#include <boost/asio.hpp>
#include <boost/make_shared.hpp>
#include <boost/enable_shared_from_this.hpp>

using namespace boost::asio;

class TcpClientConnection : public boost::enable_shared_from_this<TcpClientConnection> {
public:
    explicit TcpClientConnection(ip::tcp::socket socket);
    ~TcpClientConnection() = default;

    void start(std::string &req, std::string &res);

protected:
    void doRead();
    void onRead();
    void doWrite();
    void onWrite();

    ip::tcp::socket socket;
    boost::asio::streambuf reqBuffer;
    boost::asio::streambuf resBuffer;
    std::string* pResponse;
};

#endif //APPLICATIONSERVER_TCPCLIENTCONNECTION_H
