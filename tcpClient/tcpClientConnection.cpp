//
// Created by Филипп Исполатов on 24/11/2019.
//

#include "tcpClientConnection.h"
#include <iostream>

TcpClientConnection::TcpClientConnection(ip::tcp::socket socket) : socket(std::move(socket)) {}

void TcpClientConnection::start(std::string &req, std::string &res) {
    pResponse = &res;
    std::ostream os(&reqBuffer);
    os << req;
    doWrite();
}

void TcpClientConnection::doWrite() {
    auto self = shared_from_this();

    async_write(socket, reqBuffer, [self](boost::system::error_code err, size_t size) {
        if (!err)
            self->onWrite();
    });
}

void TcpClientConnection::onWrite() {
    doRead();
}

void TcpClientConnection::doRead() {
    auto self = shared_from_this();
    async_read(socket, resBuffer, transfer_at_least(3), [self](boost::system::error_code err, size_t size) {
        if (!err)
            self->onRead();
    });
}

void TcpClientConnection::onRead() {
    std::istream is(&resBuffer);
    is >> *pResponse;
}