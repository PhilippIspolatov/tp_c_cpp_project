//
// Created by Филипп Исполатов on 24/11/2019.
//

#include "tcpClient.h"
#include "tcpClientConnection.h"

#include <iostream>

TcpClient::TcpClient(std::string& address, int port) : service(1), socket(service) {
    auto m_address = boost::asio::ip::make_address(address);
    auto m_port = static_cast<unsigned short>(port);
    endpoint = boost::asio::ip::tcp::endpoint(m_address, m_port);
}

void TcpClient::request(std::string &req, std::string &res) {
    connect(req, res);
    service.run();
}

void TcpClient::connect(std::string &req, std::string &res) {

    socket.async_connect(endpoint, [&](const boost::system::error_code& err) {
        if (!err) {
            boost::make_shared<TcpClientConnection>(std::move(socket))->start(req, res);
        }
    });

}