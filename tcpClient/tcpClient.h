//
// Created by Филипп Исполатов on 2019-11-09.
//

#ifndef APPLICATIONSERVER_TCPCLIENT_H
#define APPLICATIONSERVER_TCPCLIENT_H

#include <boost/enable_shared_from_this.hpp>
#include <boost/asio.hpp>
#include <map>
#include <string>

using namespace boost::asio;

class TcpClient {
public:
    TcpClient(std::string& address, int port);
    void request(std::string &req, std::string &res);

private:
    void connect(std::string &req, std::string &res);

    io_service service;

    ip::tcp::socket socket;

    boost::asio::ip::tcp::endpoint endpoint;
};

#endif //APPLICATIONSERVER_TCPCLIENT_H
