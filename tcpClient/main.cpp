//
// Created by Филипп Исполатов on 24/11/2019.
//

#include <boost/asio.hpp>
#include <iostream>

#include "tcpClient.h"

using namespace boost::asio;

int main(int argc, char* argv[]) {
    std::string address = "127.0.0.1";
    int port = 5000;

    std::string req;

    while (std::cin >> req) {
        TcpClient client = TcpClient(address, port);
        client.request(req);
    }

    return 0;
}