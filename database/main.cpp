#include "models.h"
#include <unistd.h>

int main() {
    auto userModel = new UsersModel;
    auto newUser = new User;
    auto perms = new UserPerms;

    perms->addOwner("jasfaslkf", "pycnick");
    perms->addPermission("jasfaslkf", "mars");

    auto perm = perms->getUserPermissions("jasfaslkf");

    std::cout << perm.hash << std::endl;

    for (auto elem : perm.users) {
         std::cout << elem << std::endl;
    }

    perms->deletePermissions("jasfaslkf");

    newUser->username = "dasha";
    newUser->password = "dashapass";

    userModel->addUser(newUser);

    auto user = userModel->getUserByNickname("dasha");
    std::cout << user.username << " " << user.password << std::endl;

    userModel->updatePassword("dasha", "dasha");
    bool status = userModel->deleteUserByNickname("dasha");
    delete userModel;

    auto usedKeys = new UsedKeysModel;

    UsedKey newKey;
//
    newKey.key = "hfjdkgj";
    newKey.origUrl = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra ligula eget justo pharetra, eu commodo dolor consectetur. Vivamus at purus nec nisl mollis posuere et eget purus. Morbi dapibus, neque eget mollis venenatis, ipsum erat varius felis, in fermentum dolor nibh eget justo. Curabitur rutrum nec ipsum et tincidunt. Curabitur commodo libero diam, a mollis lacus pulvinar et metus.";
    newKey.username = "pycnick";
    newKey.alias = "my-google";
    newKey.expiration = 1083598325;

    usedKeys->addUsedKey(newKey);

    auto query = usedKeys->getUsedKeyByHash("hfjdkgj");

    std::cout << query.username << " " << query.alias << " " << query.origUrl << std::endl;

    auto key = usedKeys->getUsedKeyByAlias("my-vk");

    std::cout << key.username << " " << key.alias << " " << key.origUrl << std::endl;

    auto res = usedKeys->getUsedKeysByNickname("pycnick");

    for (auto k : res) {
        std::cout << k.key << std::endl;
    }

//    while (true) {
//        auto newKey = new UsedKey;
////
//        newKey->key = "asjkfhasjkfhasjkfhasjkfhasjkfhasjkfhasjkfhasjkfhasjkfhasjkfhasjkfhasjkfhasjkfhasjkfhasjkfhasjkfh";
//        newKey->origUrl = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra ligula eget justo pharetra, eu commodo dolor consectetur. Vivamus at purus nec nisl mollis posuere et eget purus. Morbi dapibus, neque eget mollis venenatis, ipsum erat varius felis, in fermentum dolor nibh eget justo. Curabitur rutrum nec ipsum et tincidunt. Curabitur commodo libero diam, a mollis lacus pulvinar et metus.";
//        newKey->username = "pycnick";
//        newKey->alias = "my-google";
//        newKey->expiration = 1083598325;
////
//        usedKeys->addUsedKey(newKey);
//
//        auto key = usedKeys->getUsedKeyByHash("asjkfhasjkfhasjkfhasjkfhasjkfhasjkfhasjkfhasjkfhasjkfhasjkfhasjkfhasjkfhasjkfhasjkfhasjkfhasjkfh");
//
//        std::cout << key.key << std::endl << key.username << std::endl << key.expiration << std::endl;
////
//        usedKeys->deleteUsedKey("asjkfhasjkfhasjkfhasjkfhasjkfhasjkfhasjkfhasjkfhasjkfhasjkfhasjkfhasjkfhasjkfhasjkfhasjkfhasjkfh");
//
//        usleep(1000000);
//    }


//
    auto set = std::vector<std::string> {"asdgadsg", "sdgds", "adsggsdg"};

    auto result = usedKeys->validateKeys(set);

    for (const auto& elem : result) {
        std::cout << elem << std::endl;
    }

    auto all = usedKeys->getAllKeys();

    for (const auto& elem : all) {
        std::cout << elem.key << std::endl;
    }

    delete newUser;
    delete usedKeys;
    return EXIT_SUCCESS;
}