#include "models.h"
#include <array>

DBMeta::DBMeta(unsigned int index, unsigned int spaceId, tnt_stream *tuple, tnt_stream *options) :
    index(index),
    spaceId(spaceId),
    limit(UINT32_MAX),
    singleTuple(tuple),
    arrayTuple(),
    options(options),
    queryStatus(0) {}

DBMeta::DBMeta(unsigned int index, unsigned int spaceId, std::vector<tnt_stream *> *tuples, tnt_stream *options) :
    index(index),
    spaceId(spaceId),
    limit(UINT32_MAX),
    singleTuple(nullptr),
    arrayTuple(tuples),
    options(options),
    queryStatus(0) {}

template<class T>
std::vector<T> DBClient<T>::select(DBMeta *meta) {
    if (db.ping()) {
        meta->setStatus(CONN_REFUSED);
        return std::vector<T>();
    }

    tnt_select(db.getConnectionStream(), meta->getSpaceId(), meta->getIndex(), meta->getLimit(), 0, 0,
               meta->getTuple());

    tnt_flush(db.getConnectionStream());

    return readReply(meta);
}

template<class T>
bool DBClient<T>::remove(DBMeta *meta) {
    if (db.ping()) {
        meta->setStatus(CONN_REFUSED);
        return false;
    }

    auto deleteTuples = meta->getArrayTuple();

    for (auto &deleteTuple : *deleteTuples) {
        tnt_delete(db.getConnectionStream(), meta->getSpaceId(), meta->getIndex(), deleteTuple);
    }
    tnt_flush(db.getConnectionStream());

    readReply(meta);

    return !meta->getStatus();
}

template<class T>
bool DBClient<T>::insert(DBMeta *meta) {
    if (db.ping()) {
        meta->setStatus(CONN_REFUSED);
        return false;
    }
    auto insertTuples = meta->getArrayTuple();

    for (auto &insertTuple : *insertTuples) {
        tnt_insert(db.getConnectionStream(), meta->getSpaceId(), insertTuple);
    }
    tnt_flush(db.getConnectionStream());

    readReply(meta);

    return !meta->getStatus();
}

template<class T>
bool DBClient<T>::update(DBMeta *meta) {
    if (db.ping()) {
        meta->setStatus(CONN_REFUSED);
        return false;
    }

    auto updateOptions = meta->getOptions();
    auto updateTuple = meta->getTuple();

    tnt_update(db.getConnectionStream(), meta->getSpaceId(), meta->getIndex(), updateTuple, updateOptions);
    tnt_flush(db.getConnectionStream());

    readReply(meta);

    return !meta->getStatus();
}

template<class T>
std::vector<T> DBClient<T>::readReply(DBMeta *&meta) {
    struct tnt_reply reply{};
    tnt_reply_init(&reply);
    db.getConnectionStream()->read_reply(db.getConnectionStream(), &reply);

    if (reply.code != 0) {
        tnt_reply_free(&reply);
        meta->setStatus(BAD_META);
        return std::vector<T>();
    }

    msgpack::object_handle oh = msgpack::unpack(reply.data, reply.data_end - reply.data);
    return oh.get().as<std::vector<T>>();
}

User UsersModel::getUserByNickname(const std::string &nickname) {
    tnt_stream *tuple = tnt_object(nullptr);
    tnt_object_format(tuple, "[%s]", nickname.c_str());
    auto meta = new DBMeta(0, 512, tuple, nullptr);

    auto query = select(meta);

    tnt_stream_free(tuple);
    if (meta->getStatus() == 0) {
        delete meta;
        if (query.size() == 1)
            return query.front();
        else
            return User();
    }

    delete meta;
    return User();
}

bool UsersModel::deleteUserByNickname(const std::string &nickname) {
    tnt_stream *tuple = tnt_object(nullptr);
    tnt_object_format(tuple, "[%s]", nickname.c_str());

    auto deleteTuples = new std::vector<tnt_stream *>{tuple};
    auto meta = new DBMeta(0, 512, deleteTuples, nullptr);
    remove(meta);

    bool status = meta->getStatus();

    tnt_stream_free(tuple);
    delete meta;
    delete deleteTuples;
    return !status;
}

bool UsersModel::addUser(User *user) {
    tnt_stream *tuple = tnt_object(nullptr);
    tnt_object_format(tuple, "[%s%s]", user->username.c_str(), user->password.c_str());

    auto insertTuples = new std::vector<tnt_stream *>{tuple};
    auto meta = new DBMeta(0, 512, insertTuples, nullptr);
    insert(meta);

    bool status = meta->getStatus();

    tnt_stream_free(tuple);
    delete meta;
    delete insertTuples;
    return !status;
}

bool UsersModel::updatePassword(const std::string &username, const std::string &newPassword) {
    tnt_stream *tuple = tnt_object(nullptr);
    tnt_stream *options = tnt_update_container(nullptr);

    tnt_object_format(tuple, "[%s]", username.c_str());
    tnt_update_splice(options, 1, 0, INT8_MAX, newPassword.c_str(), newPassword.size());
    tnt_update_container_close(options);

    auto meta = new DBMeta(0, 512, tuple, options);

    update(meta);

    bool status = meta->getStatus();

    tnt_stream_free(tuple);
    tnt_stream_free(options);
    delete meta;
    return !status;
}

UsedKey UsedKeysModel::getUsedKeyByHash(const std::string &hash) {
    tnt_stream *tuple = tnt_object(nullptr);
    tnt_object_format(tuple, "[%s]", hash.c_str());
    auto meta = new DBMeta(0, 513, tuple, nullptr);
    auto query = select(meta);

    tnt_stream_free(tuple);

    if (meta->getStatus() == 0) {
        delete meta;
        if (query.size() == 1)
            return query.front();
        else
            return UsedKey();
    }

    delete meta;
    return UsedKey();
}

UsedKey UsedKeysModel::getUsedKeyByAlias(std::string alias) {
    tnt_stream *tuple = tnt_object(nullptr);
    tnt_object_format(tuple, "[%s]", alias.c_str());
    auto meta = new DBMeta(2, 513, tuple, nullptr);
    auto query = select(meta);

    tnt_stream_free(tuple);
    if (meta->getStatus() == 0) {
        delete meta;
        if (query.size() == 1)
            return query.front();
        else
            return UsedKey();
    }

    delete meta;
    return UsedKey();
}

std::vector<UsedKey> UsedKeysModel::getUsedKeysByNickname(std::string nickname) {
    tnt_stream *tuple = tnt_object(nullptr);
    tnt_object_format(tuple, "[%s]", nickname.c_str());
    auto meta = new DBMeta(1, 513, tuple, nullptr);
    auto query = select(meta);

    tnt_stream_free(tuple);
    delete meta;
    return query;
}

bool UsedKeysModel::addUsedKey(UsedKey &uKey) {
    tnt_stream *tuple = tnt_object(nullptr);
    tnt_object_format(tuple, "[%s%s%s%s%d]", uKey.key.c_str(), uKey.origUrl.c_str(),
                      uKey.username.c_str(),
                      uKey.alias.c_str(),
                      uKey.expiration);

    auto insertTuples = new std::vector<tnt_stream *>{tuple};
    auto meta = new DBMeta(0, 513, insertTuples, nullptr);
    insert(meta);

    bool status = meta->getStatus();

    tnt_stream_free(tuple);
    delete meta;
    delete insertTuples;
    return !status;
}

std::vector<std::string> UsedKeysModel::validateKeys(std::vector<std::string> keys) {
    struct tnt_request *request = tnt_request_call_16(NULL);
    struct tnt_stream *arguments = tnt_object(NULL);

    tnt_object_add_array(arguments, 1);
    tnt_object_add_array(arguments, keys.size());

    for (auto key : keys) {
        tnt_object_add_strz(arguments, key.c_str());
    }

    tnt_call(db.getConnectionStream(), "validate_keys", 13, arguments);
    tnt_flush(db.getConnectionStream());

    struct tnt_reply reply{};
    tnt_reply_init(&reply);
    db.getConnectionStream()->read_reply(db.getConnectionStream(), &reply);

    if (reply.code != 0) {
        return std::vector<std::string>();
    }

    msgpack::object_handle oh = msgpack::unpack(reply.data, reply.buf_size);
    msgpack::object deserialized = oh.get();

    tnt_reply_free(&reply);
    tnt_request_free(request);
    tnt_stream_free(arguments);

    auto result = deserialized.as<std::vector<std::vector<std::string>>>();

    return result.front();
}

bool UsedKeysModel::deleteUsedKey(std::string key) {
    tnt_stream *tuple = tnt_object(nullptr);
    tnt_object_format(tuple, "[%s]", key.c_str());

    auto deleteTuples = new std::vector<tnt_stream *>{tuple};
    auto meta = new DBMeta(0, 513, deleteTuples, nullptr);
    remove(meta);

    bool status = meta->getStatus();

    tnt_stream_free(tuple);
    delete meta;
    delete deleteTuples;
    return !status;
}

std::vector<UsedKey> UsedKeysModel::getAllKeys() {
    tnt_stream *tuple = tnt_object(nullptr);
    tnt_object_format(tuple, "[]");

    auto meta = new DBMeta(1, 513, tuple, nullptr);
    auto query = select(meta);

    tnt_stream_free(tuple);
    delete meta;
    return query;
}

UserPermissions UserPerms::getUserPermissions(const std::string &hash) {
    tnt_stream *tuple = tnt_object(nullptr);
    tnt_object_format(tuple, "[%s]", hash.c_str());
    auto meta = new DBMeta(0, 514, tuple, nullptr);

    auto query = select(meta);

    tnt_stream_free(tuple);
    if (meta->getStatus() == 0) {
        delete meta;
        if (query.size() == 1)
            return query.front();
        else
            return UserPermissions();
    }

    delete meta;
    return UserPermissions();
}

bool UserPerms::addOwner(const std::string &hash, const std::string &owner) {
    tnt_stream *tuple = tnt_object(nullptr);
    tnt_object_format(tuple, "[%s[%s]]", hash.c_str(), owner.c_str());

    auto insertTuples = new std::vector<tnt_stream *>{tuple};
    auto meta = new DBMeta(0, 514, insertTuples, nullptr);
    insert(meta);

    bool status = meta->getStatus();

    tnt_stream_free(tuple);
    delete meta;
    delete insertTuples;
    return !status;
}

bool UserPerms::addPermission(const std::string &hash, const std::vector<std::string> &users) {
    tnt_stream *tuple = tnt_object(nullptr);
    tnt_stream *options = tnt_update_container(nullptr);

    tnt_object_format(tuple, "[%s]", hash.c_str());

    auto meta = new DBMeta(0, 514, tuple, nullptr);
    auto key = select(meta);

    if (meta->getStatus() != 0) {
        delete meta;
        return false;
    }

    if (key.size() == 0)
        return false;

    auto currUsers = key.front().users;

    for (auto user : users) {
        currUsers.push_back(user);
    }

    struct tnt_stream *arguments = tnt_object(NULL);

    tnt_object_add_array(arguments, currUsers.size());

    for (auto user : currUsers) {
        tnt_object_add_strz(arguments, user.c_str());
    }

    tnt_update_assign(options, 1, arguments);
    tnt_update_container_close(options);

    meta = new DBMeta(0, 514, tuple, options);

    update(meta);
    bool status = meta->getStatus();

    tnt_stream_free(arguments);
    tnt_stream_free(options);
    delete meta;
    return !status;
}

bool UserPerms::deletePermissions(const std::string &hash) {
    tnt_stream *tuple = tnt_object(nullptr);
    tnt_object_format(tuple, "[%s]", hash.c_str());

    auto deleteTuples = new std::vector<tnt_stream *>{tuple};
    auto meta = new DBMeta(0, 514, deleteTuples, nullptr);
    remove(meta);

    bool status = meta->getStatus();

    tnt_stream_free(tuple);
    delete meta;
    delete deleteTuples;
    return !status;
}
