#ifndef PROGRAM_DATABASE_H
#define PROGRAM_DATABASE_H

#include  <iostream>

#include <tarantool/tarantool.h>
#include <tarantool/tnt_net.h>
#include <tarantool/tnt_opt.h>

// Mayer's singleton
class Database {
public:
  static Database &getInstance();
  struct tnt_stream *getConnectionStream();
  int ping();

private:
  Database();
  ~Database();

  Database(Database const &) = delete;
  Database &operator= (Database const&) = delete;

  virtual void connect();

  int connectionStatus = 0;
  struct tnt_stream *connectionStream;
};

#endif //PROGRAM_DATABASE_H
