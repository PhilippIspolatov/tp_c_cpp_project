console = require('console')

console.listen('unix/:/var/run/tarantool/tarantool.sock')

box.cfg{
    listen = 3301
}

users = box.schema.space.create('users', {if_not_exists = true})
keys = box.schema.space.create('usedKeys', {if_not_exists = true})
permissions = box.schema.space.create('permissions', {if_not_exists = true})
box.schema.func.create('validate_keys', {language = 'LUA'}, {if_not_exists = true})

if not box.schema.func.exists('validate_keys') then
    function validate_keys(keys)
        local valid = {}
        for i = 1, #keys do
            if not box.space.usedKeys:get(keys[i]) then
                table.insert(valid, keys[i])
            end
        end
        return valid
    end
end

box.schema.user.grant('guest','read,write','space','users', {if_not_exists = true})
box.schema.user.grant('guest','read,write','space','usedKeys', {if_not_exists = true})
box.schema.user.grant('guest','read,write','space','permissions', {if_not_exists = true})
box.schema.user.grant('guest','execute','function','validate_keys', {if_not_exists = true})

users:format({
    {name = 'username', type = 'string'},
    {name = 'password', type = 'string'},
})

users:create_index('primary', {
    unique = true,
    type = 'HASH',
    parts = {'username'},
    if_not_exists = true
})

permissions:format({
    {name = 'hash', type = 'string'},
    {name = 'users', type = 'array'},
})

permissions:create_index('primary', {
    unique = true,
    type = 'HASH',
    parts = {'hash'},
    if_not_exists = true
})

keys:format({
    {name = 'hash', type = 'string'},
    {name = 'url', type = 'string'},
    {name = 'username', type = 'string'},
    {name = 'alias', type = 'string'},
    {name = 'ttl', type = 'unsigned'}
})

keys:create_index('primary', {
    unique = true,
    type = 'HASH',
    parts = {'hash'},
    if_not_exists = true
})

keys:create_index('byUser', {
    unique = false,
    type = 'TREE',
    parts = {'username'},
    if_not_exists = true
})

keys:create_index('byAlias', {
    unique = true,
    type = 'HASH',
    parts = {'alias'},
    if_not_exists = true
})

console.start()
