#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "models.h"

using ::testing::AtLeast;
using ::testing::DoAll;
using ::testing::Return;
using ::testing::SetArgReferee;

class MockDBUserClient : public DBClient<User> {
public:
    MOCK_METHOD(std::vector<User *>, select, (DBMeta *));
    MOCK_METHOD(std::vector<User *>, insert, (DBMeta *));
    MOCK_METHOD(std::vector<User *>, remove, (DBMeta *));
    MOCK_METHOD(std::vector<User *>, update, (DBMeta *));
};

TEST(Models, select) {
    MockDBUserClient client;
    DBMeta *meta = new DBMeta(0, nullptr, nullptr);
    EXPECT_CALL(client, select(meta)).Times(AtLeast(1));

    UsersModel users;
    users.getUserByNickname("pycnick");
}

TEST(Database, DBMeta) {
    tnt_stream *stream = nullptr;
    tnt_stream *options = nullptr;
    tnt_object(stream);
    tnt_object(options);
    int index = 1;

    auto meta = DBMeta(index, stream, options);

    EXPECT_EQ(meta.getIndex(), index);
    EXPECT_EQ(meta.getTuple(), stream);
    EXPECT_EQ(meta.getOptions(), options);
}


TEST(Models, Users) {
    auto users = new UsersModel;
    auto nickname = users->getUserByNickname("pycnick");

    if (nickname != nullptr) {
        EXPECT_STREQ(nickname->username.c_str(), "pycnick");
    }
}

TEST(Models, UsedKeys) {
    auto keys = new UsedKeysModel;
    auto alias = keys->getUsedKeyByAlias("my-vk");
    auto key = keys->getUsedKeyByHash("auryeh");

    if (alias != nullptr && key != nullptr) {
        EXPECT_STREQ(alias->alias.c_str(), "my-vk");
        EXPECT_STREQ(alias->key.c_str(), "auryeh");
        EXPECT_STREQ(key->alias.c_str(), "my-vk");
        EXPECT_STREQ(key->key.c_str(), "auryeh");
    }
}

int main(int argc, char **argv) {
    // The following line must be executed to initialize Google Mock
    // (and Google Test) before running the tests.
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}