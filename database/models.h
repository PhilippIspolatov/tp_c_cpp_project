#ifndef PROGRAM_MODELS_H
#define PROGRAM_MODELS_H

#include "database.h"
#include <utility>
#include <unordered_set>
#include <unordered_map>
#include <msgpack.hpp>

#define CONN_REFUSED -2
#define BAD_META -3

struct KeyQuery {
    KeyQuery() = default;
    KeyQuery(std::string username, std::string origUrl, std::string alias) : username(username),
                                                                                origUrl(origUrl),
                                                                                alias(alias) {}

    std::string username;
    std::string origUrl;
    std::string alias;

    MSGPACK_DEFINE (username, origUrl, alias);
};

struct KeyResponse {
    std::string hash;

    MSGPACK_DEFINE (hash);
};

struct UserPermissions {
    UserPermissions() = default;

    UserPermissions(std::string &hash, std::vector<std::string> &users) : hash(hash), users(users) {}

    std::string hash;
    std::vector<std::string> users;

    MSGPACK_DEFINE (hash, users);

};


class DBMeta {
public:
    DBMeta(unsigned int index, unsigned int spaceId, tnt_stream *tuple, tnt_stream *options);

    DBMeta(unsigned int index, unsigned int spaceId, std::vector<tnt_stream *> *tuples, tnt_stream *options);

    ~DBMeta() = default;

    unsigned int getIndex() {
        return index;
    }

    size_t getLimit() {
        return limit;
    }

    unsigned int getSpaceId() {
        return spaceId;
    }

    tnt_stream *getTuple() {
        return singleTuple;
    }

    std::vector<tnt_stream *> *getArrayTuple() {
        return arrayTuple;
    }

    tnt_stream *getOptions() {
        return options;
    }

    int getStatus() {
        return queryStatus;
    }

    void setStatus(int status) {
        queryStatus = status;
    }

private:
    unsigned int index;
    unsigned int spaceId;
    size_t limit;
    tnt_stream *singleTuple;
    std::vector<tnt_stream *> *arrayTuple;
    tnt_stream *options;
    int queryStatus;
};

template<class T>
class DBClient {
public:
    DBClient() : db(Database::getInstance()) {}
    virtual ~DBClient() = default;
protected:
    std::vector<T> select(DBMeta *meta);

    bool insert(DBMeta *meta);

    bool remove(DBMeta *meta);

    bool update(DBMeta *meta);

    std::vector<T> readReply(DBMeta *&meta);

    Database &db;
};

struct User {
    User() = default;

    User(std::string &uname, std::string &pass) : username(uname), password(pass) {}

    std::string username;
    std::string password;

    MSGPACK_DEFINE (username, password);
};

class UsersModel : public DBClient<User> {
public:
    virtual ~UsersModel() = default;
    User getUserByNickname(const std::string &nickname);

    bool deleteUserByNickname(const std::string &nickname);

    bool addUser(User *user);

    bool updatePassword(const std::string &username, const std::string &newPassword);
};

struct UsedKey {
    UsedKey() = default;

    UsedKey(std::string &key, std::string &origUrl, std::string &username, std::string &alias, unsigned int &exp) :
        key(key), origUrl(origUrl), username(username), alias(alias), expiration(exp) {}

    std::string key;
    std::string origUrl;
    std::string username;
    std::string alias;
    unsigned int expiration;

    MSGPACK_DEFINE (key, origUrl, username, alias, expiration);
};

class UsedKeysModel : public DBClient<UsedKey> {
public:
    virtual ~UsedKeysModel() = default;
    std::vector<UsedKey> getAllKeys();

    UsedKey getUsedKeyByHash(const std::string &hash);

    UsedKey getUsedKeyByAlias(std::string alias);

    std::vector<UsedKey> getUsedKeysByNickname(std::string nickname);

    bool addUsedKey(UsedKey &uKey);

    bool deleteUsedKey(std::string key);

    std::vector<std::string> validateKeys(std::vector<std::string> keys);
};

class UserPerms : public DBClient<UserPermissions> {
public:
    virtual ~UserPerms() = default;
    UserPermissions getUserPermissions(const std::string &hash);

    bool addPermission(const std::string &hash, const std::vector<std::string> &users);

    bool addOwner(const std::string &hash, const std::string &owner);

    bool deletePermissions(const std::string &hash);
};

#endif //PROGRAM_MODELS_H
