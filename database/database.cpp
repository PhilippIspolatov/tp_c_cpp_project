#include "database.h"

Database::Database() {
  connectionStream = tnt_net(nullptr);
  connect();
}

Database::~Database() {
  tnt_close(connectionStream);
  tnt_stream_free(connectionStream);
}

Database& Database::getInstance() {
  static Database instance;
  return instance;
}

struct tnt_stream *Database::getConnectionStream() {
  return connectionStream;
}

int Database::ping() {
  return connectionStatus;
}

void Database::connect() {
  tnt_set(connectionStream, TNT_OPT_URI, "127.0.0.1:3301");
  connectionStatus = tnt_connect(connectionStream);
}