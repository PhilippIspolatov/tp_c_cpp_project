#include <iostream>
#include "cleaner.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

using ::testing::DoAll;
using ::testing::AtLeast;

class MockDaemon: public Daemon {
public:
    MOCK_METHOD(void, Run, ());
    MOCK_METHOD(int, Monitoring, ());
    MOCK_METHOD(int, Daemon_work, ());
};

class MockCleaner: public Cleaner {
public:
    MockCleaner() : Cleaner (nullptr, nullptr){}
    MOCK_METHOD (void, Set_time, (time_t t));
    MOCK_METHOD (int, Clean, ());
    MOCK_METHOD (void, Sleep, ());
};

class MockUsedKeys: public UsedKeysModel
{
public:
    MOCK_METHOD (std::vector<std::string>, select, (std::string));
};


TEST(Daemon, Monitoring) {
    MockDaemon daemon;
    EXPECT_CALL(daemon,Monitoring()).Times(AtLeast(1));
    Cleaner cleaner (nullptr, nullptr);
    cleaner.Run();
    //EXPECT_TRUE(cleaner.workingStatus != 0);
}

TEST(Daemon, Daemon_work) {
    MockDaemon daemon;
    EXPECT_CALL(daemon,Daemon_work()).Times(AtLeast(1));
    Cleaner cleaner(nullptr, nullptr);
    cleaner.Run();
}


TEST (Cleaner, Get_parameters_Set_time)
{
    MockCleaner m_cleaner;
    std::vector <std::map <std::string, std::string>> param;
    param[0]["time"] = "5";
    m_cleaner.Get_parameters(param);

    EXPECT_CALL(m_cleaner, Set_time(5)).Times(1);
}

TEST (Cleaner, Get_parameters_time) {
    Cleaner cleaner(nullptr, nullptr);
    std::vector <std::map <std::string, std::string>> param;
    param[0]["time"] = "5";
    cleaner.Get_parameters(param);
    EXPECT_EQ(cleaner.T, 5);
}

TEST (Cleaner, Get_parameters_command) {
    Cleaner cleaner(nullptr, nullptr);
    std::vector <std::map <std::string, std::string>> param;
    param[0]["command"] = "once";
    cleaner.Get_parameters(param);
    EXPECT_EQ(cleaner.cleaner_status, "busy");
}

TEST (Cleaner, Clean_call) {
    MockCleaner m_cleaner;
    std::vector <std::map <std::string, std::string>> param;
    param[0]["command"] = "once";
    EXPECT_CALL(m_cleaner, Clean()).Times(AtLeast(1));

    m_cleaner.Get_parameters(param);
}


TEST (Cleaner, Connect_db)
{
    Cleaner cleaner(nullptr, nullptr);
    cleaner.Run();
    EXPECT_TRUE(cleaner.uKeys != nullptr);
    EXPECT_TRUE(cleaner.unKeys != nullptr);
}

TEST (Cleaner, Loop_clean)
{
    MockCleaner m_cleaner;
    EXPECT_CALL(m_cleaner, Clean()).Times((AtLeast(1)));
    EXPECT_CALL(m_cleaner, Sleep()).Times((AtLeast(1)));

    std::vector <std::map <std::string, std::string>> param;
    param[0]["command"] = "loop";
    m_cleaner.Get_parameters(param);
}


TEST (Cleaner, Recover_keys)
{
    MockUsedKeys m_uKeys;
    Cleaner cleaner(&m_uKeys, nullptr);
    cleaner.Clean();
    std::string request;

    EXPECT_CALL(m_uKeys, select(request)).Times(AtLeast(1));
}

TEST (Cleaner, del_UsedKeys)
{
    Cleaner cleaner (nullptr, nullptr);
    cleaner.Clean();

    DBMeta *meta = new DBMeta(0, nullptr, nullptr);
    std::vector<UsedKey*> recoverRecords = cleaner.uKeys->select(meta);

    EXPECT_TRUE(recoverRecords.empty());
}


int main(int argc, char **argv) {
   ::testing::InitGoogleTest(&argc, argv);
   return RUN_ALL_TESTS();
}

