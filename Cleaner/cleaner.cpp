#include "cleaner.h"

time_t Cleaner::param_Sleep_time = 3600*24;   //default = 24h
std::string Cleaner::param_Mode = "Loop";
time_t Cleaner::param_Time = 0;

time_t Cleaner::urlLifeTime = 3600*24;


bool Cleaner::Set_sleep_time (time_t t=3600*24)
{
    time_t time_min = 5;
    if (t < time_min)
    {
        printf("Ошибка параметров: минимальное значение Т = %ld\n", time_min);
        return false;
    }
    param_Sleep_time = t;
    return true;
}

bool Cleaner::Set_mode(const std::string& mode="loop")
{
    if (mode.empty()) return false;
    param_Mode=mode;
    return true;
}


bool Cleaner::Set_time(time_t time=0)
{
    if (time==0)
    {
       param_Time = std::time(nullptr);
       return true;
    }

    if (time>=std::time(nullptr))
    {
        param_Time = time;
        return true;
    }
    return false;
}


Cleaner::Cleaner() {
    Set_sleep_time();
    Set_time();
    Set_mode();
}


time_t Cleaner::Get_Time(const char *str_time)
{
    if (strlen(str_time)>6)
    {
        printf("Неверный формат времени\n");
        return -1;
    }

    char time_buf[6];
    strcpy(time_buf, ++str_time);

    char *pEnd;
    int hour = (int)strtol(time_buf, &pEnd, 10);

    if (pEnd==time_buf) return -1;
    if ((hour<0)||(hour>23))
    {
        printf("Неверный формат времени\n");
        return -1;
    }

    strcpy(time_buf, ++pEnd);
    int min = (int)strtol(time_buf, &pEnd, 10);

    if (pEnd==time_buf) return -1;

    if ((min<0)||(min>59))
    {
        printf("Неверный формат времени\n");
        return -1;
    }

    time_t current_time = std::time(nullptr);
    tm *time_struct = localtime(&current_time);

    time_struct->tm_hour = hour;
    time_struct->tm_min = min;
    time_struct->tm_sec = 0;

    time_t setted_time = mktime(time_struct);
    if (setted_time<current_time) setted_time+=24*3600;

    return setted_time;
}


int Cleaner::Daemon_work()
{
    if (param_Mode=="loop")
    {
        syslog(LOG_LOCAL0|LOG_INFO,"циклическая чистка"); //delete

        while (true)
        {
            time_t t = std::time(nullptr);
            if (t>=param_Time)
            {
                Clean();
                Set_time(param_Time+param_Sleep_time);
            }
            if (ShutdownFinishWork==1) break;
            sleep(1);
        }
    }

    if (param_Mode=="once")
    {
        syslog(LOG_LOCAL0|LOG_INFO,"одиночная чистка"); //delete
        Clean();
        ShutdownFinishWork=1;
        return 0;
    }
}


int Cleaner::Get_parameters(int argc, char** argv)
{
    time_t get_Sleep_Time=3600*24;
    std::string get_Mode="loop";
    time_t get_Time = 0;

    int flag_T = 0;

    for (int i=1; i<argc; i++)
    {
        if (flag_T==1) {
            if (strtol(argv[i], nullptr, 10) > 0) {
                get_Sleep_Time =strtol(argv[i], nullptr, 10);
                flag_T = 0;
                continue;
            }
            printf("Неверный формат времени ожидания %s\n", argv[i]);
            return -1;
        }

        if (argv[i][0]=='-')
        {
            if (strcmp(argv[i],"-loop")==0)
            {
                get_Mode="loop";
                continue;
            }

            if (strcmp(argv[i],"-once")==0)
            {
                get_Mode="once";
                continue;
            }

            if (strcmp(argv[i],"-T")==0)
            {
                flag_T=1;
                continue;
            }

            get_Time = Get_Time(argv[i]);
            if (get_Time!=-1) continue;
            else
            {
                printf("Неизвестный параметр %s\n", argv[i]);
                return -1;
            }
        }

    printf("Неверный формат параметров\n");
    return -1;
    }

    if ((Set_sleep_time(get_Sleep_Time))&&(Set_mode(get_Mode))&&(Set_time(get_Time)))  return 0;

    Set_sleep_time();
    Set_mode();
    Set_time();

    return -1;
}



int Cleaner::Clean()
{
    DaemonBusy = 0; //функция, созвращающая состояние бд
    time_t timeout = 0;
    time_t waiting_time = 10;

    while (DaemonBusy==1)
    {
        if (timeout>param_Sleep_time/2)
        {
            syslog(LOG_LOCAL0|LOG_INFO,"время ожидания истекло, чистка отменена");
            return 0;
        }
        sleep(waiting_time);
        timeout+=waiting_time;

        DaemonBusy = 0; //функция, созвращающая состояние бд
    }
    Delete_expired_key();
    return 0;
}



void Cleaner::Delete_expired_key() {

    auto used_keys = new UsedKeysModel;
    auto all_keys = used_keys->getAllKeys();

    if (all_keys.empty())
    {
        syslog(LOG_LOCAL0|LOG_INFO,"Удалено 0 записей");
        return;
    }

    int current_time = std::time(nullptr);
    int count = 0;

    for (const auto& item : all_keys) {
        if((item.expiration+urlLifeTime)<current_time)
        {
            if (used_keys->deleteUsedKey(item.key))
            {
                count++;
                continue;
            }
            syslog(LOG_LOCAL0|LOG_INFO,"Не удалось удалить запись %s из базы данных", item.key.c_str());
        }
    }
    syslog(LOG_LOCAL0|LOG_INFO,"Удалено %d записей", count);
}

