#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <unistd.h>
#include <fcntl.h>
#include <csignal>
#include <syslog.h>
#include <cerrno>
#include <sys/stat.h>

#include "daemon.h"

const char* Daemon::PidFilePath="/var/run/cleaner.pid";
int	Daemon::PidFileDescriptor=-1;
sig_atomic_t Daemon::ShutdownFinishWork=0;
sig_atomic_t Daemon::RestartWork=0;
sig_atomic_t Daemon::DaemonBusy=0;


void * Daemon:: Run(int argc, char** argv)
{
    if (argc==1) Start();

    if (argc > 1) {
        int pid_file = open(PidFilePath, O_RDONLY);

        if (pid_file < 0) {

            if (argv[1][0] == '-') {
                if (Get_parameters(argc, argv) == 0) {
                    Start();
                } else {
                    printf("Не удалось запустить процесс: ошибка параметров\n");
                    exit(EXIT_FAILURE);
                }
            } else {
                perror("Файл блокировки не найден. Возможно, программа не запущена?\n");
                exit(pid_file);
            }
        } else {
            char pid_buffer[16];
            int f_lenght = read(pid_file, pid_buffer, 16);
            pid_buffer[f_lenght] = 0;

            pid_t pid = strtol(pid_buffer, nullptr, 10);
            if (!pid) {
                printf("Не удалось получить pid из файла\n");
                exit(pid_file);
            }

            if (strcmp(argv[1], "finish") == 0){
                kill(pid, SIGUSR1);
                exit(EXIT_SUCCESS);
            }

            if (strcmp(argv[1], "stop") == 0) {
                kill(pid, SIGTERM);
                exit(EXIT_SUCCESS);
            }

            if (strcmp(argv[1], "restart") == 0) {
                kill(pid, SIGHUP);
                exit(EXIT_SUCCESS);
            } else {
                printf("Возможные команды %s [finish|stop|restart]\n", argv[0]);
                exit(EXIT_FAILURE);
            }
        }
    }
}


void Daemon::Start()
{
    int result;
    pid_t daemon_PID;

    result = Become_daemon(PidFilePath, "cleaner", LOG_DEBUG, &PidFileDescriptor);
    if (result<0)
    {
        perror ("Не удалось запустить процесс в режиме демона");
        exit (result);
    }

    result = Signal_handling();
    if (result<0)
    {
        syslog(LOG_LOCAL0|LOG_INFO,"Не удалось настроить обработчик сигналов, errno=%d",errno);
        unlink(PidFilePath);
        exit(result);
    }


    while(true)
    {
        Daemon_work();

        if((ShutdownFinishWork==1)&&(RestartWork==0)) break;
        ShutdownFinishWork=RestartWork=0;
    }

    syslog(LOG_LOCAL0|LOG_INFO,"Процесс завершен");
    Close_Descriptors();
}



int Daemon::Become_daemon(const char *const pidFileName, const char *const logPrefix, const int logLevel, int *const pidFileDescriptor)
{
    chdir("/");

    int lockFileDesc=open(pidFileName,O_CREAT|O_RDWR|O_EXCL, 0644);

    if (lockFileDesc==-1) //если не получилось, файл либо не существует, либо он еще закрыт
    {
        FILE *lfp = fopen(pidFileName, "r");
        if (lfp==0)
        {
            perror ("Не удалось найти файл блокировки");
            return -1;
        }

        char pid_buffer[17];
        unsigned long lock_Pid;
        char* lfs=fgets(pid_buffer,16, lfp);

        if(lfs!=0)
        {
            if(pid_buffer[strlen(pid_buffer)-1]=='\n') pid_buffer[strlen(pid_buffer)-1]=0;
            lock_Pid=strtol(pid_buffer, nullptr,10);

            if (kill(lock_Pid,0)==0) //если процесс существует
            {
                printf("\n\nERROR\n\nНайден файл блокировки %s, принадлежащий процессу %ld.\n\n",pidFileName, lock_Pid);
            }

            else
            {
                if(errno==ESRCH)
                {
                    printf("\n\nERROR\n\nНайден файл блокировки%s, принадлежащий несуществующему процессу %ld.\nУдалите файл и попробуйте ещё раз.\n\n",pidFileName,lock_Pid);
                }
                else perror("Не удалось получить доступ к файлу блокировки");

            }
        }

        else perror("Не удалось получить доступ к файлу блокировки");

        fclose(lfp);
        return -1;
    }


    struct flock exclusiveLock;

    exclusiveLock.l_len=0;
    exclusiveLock.l_start=0;
    exclusiveLock.l_pid=0;
    exclusiveLock.l_type=F_WRLCK;
    exclusiveLock.l_whence=SEEK_SET;

    if (fcntl(lockFileDesc,F_SETLK,&exclusiveLock)<0)
    {
        close(lockFileDesc);
        perror("Не удалось получить доступ к файлу блокировки");
        return -1;
    }

    int current_Pid = fork();

    switch (current_Pid)
    {
        case 0: //если мы дочерний процесс
            break;

        case -1:
            fprintf(stderr,"Не удалось создать дочерний процесс: %s\n", strerror(errno));
            return -1;

        default:
            exit(0);
    }

    if (setsid()<0) return -1;
    if(ftruncate(lockFileDesc,0)<0) return -1;

    char str_pid[7];
    sprintf(str_pid,"%d\n",(int)getpid());
    write(lockFileDesc,str_pid,strlen(str_pid));

    *pidFileDescriptor=lockFileDesc;

    int numFiles = (int)sysconf(_SC_OPEN_MAX);
    for(int i=0; i<numFiles; ++i)
    {
        if(i!=lockFileDesc) close(i);
    }

    umask(0);

//вернуть дескрипторы ввода/вывода/ошибок
    int stdioFD=open("/dev/null",O_RDWR);
    dup(stdioFD);
    dup(stdioFD);

    openlog(logPrefix,LOG_PID|LOG_CONS|LOG_NDELAY|LOG_NOWAIT,LOG_LOCAL0);
    (void)setlogmask(LOG_UPTO(logLevel));

    return 0;
}



int Daemon::Signal_handling()
{
    signal(SIGUSR2, SIG_IGN); //сигналы, которые игнорируем
    signal(SIGPIPE, SIG_IGN);
    signal(SIGALRM, SIG_IGN);
    signal(SIGTSTP, SIG_IGN);
    signal(SIGTTIN, SIG_IGN);
    signal(SIGTTOU, SIG_IGN);
    signal(SIGURG, SIG_IGN);
    signal(SIGXCPU, SIG_IGN);
    signal(SIGXFSZ, SIG_IGN);
    signal(SIGVTALRM, SIG_IGN);
    signal(SIGPROF, SIG_IGN);
    signal(SIGIO, SIG_IGN);
    signal(SIGCHLD, SIG_IGN);

    signal(SIGQUIT, Fatal_Sig_Handler); //которые регистрируются и попадают в лог
    signal(SIGILL, Fatal_Sig_Handler);
    signal(SIGTRAP, Fatal_Sig_Handler);
    signal(SIGABRT, Fatal_Sig_Handler);
    signal(SIGIOT, Fatal_Sig_Handler);
    signal(SIGBUS, Fatal_Sig_Handler);
    signal(SIGFPE, Fatal_Sig_Handler);
    signal(SIGSEGV, Fatal_Sig_Handler);
//    signal(SIGSTKFLT, Fatal_Sig_Handler);
    signal(SIGCONT, Fatal_Sig_Handler);
//    signal(SIGPWR, Fatal_Sig_Handler);
    signal(SIGSYS, Fatal_Sig_Handler);
#ifdef SIGEMT
    signal(SIGEMT,Fatal_Sig_Handler);
#endif

    struct sigaction sighupSA;
    struct sigaction sigusr1SA;
    struct sigaction sigtermSA;

    sigtermSA.sa_handler=Term_Handler;
    sigemptyset(&sigtermSA.sa_mask);
    sigtermSA.sa_flags=0;
    sigaction(SIGTERM,&sigtermSA, nullptr);

    sigusr1SA.sa_handler=Usr1_Handler; //завершить обр-ку сигналов перед остановкой
    sigemptyset(&sigusr1SA.sa_mask);
    sigusr1SA.sa_flags=0;
    sigaction(SIGUSR1,&sigusr1SA, nullptr);

    sighupSA.sa_handler=Hup_Handler; //перезапуститься
    sigemptyset(&sighupSA.sa_mask);
    sighupSA.sa_flags=0;
    sigaction(SIGHUP,&sighupSA, nullptr);

    return 0;
}



void Daemon::Fatal_Sig_Handler(int sig)
{
#ifdef _GNU_SOURCE
    syslog(LOG_LOCAL0|LOG_INFO,"принят сигнал: %s",strsignal(sig));
#else
    syslog(LOG_LOCAL0|LOG_INFO,"принят сигнал: %d ",sig);
#endif
    closelog();
    Close_Descriptors();
    _exit(0);
}



void Daemon::Term_Handler(int sig)
{
    syslog(LOG_LOCAL0|LOG_INFO,"принят сигнал SIGTERM, процесс был прерван");
    Close_Descriptors();
    _exit(0);
}


void Daemon::Hup_Handler(int sig)
{
    syslog(LOG_LOCAL0|LOG_INFO,"принят сигнал SIGHUP");
    ShutdownFinishWork=1;
    RestartWork=1;
}


void Daemon::Usr1_Handler(int sig)
{
    syslog(LOG_LOCAL0|LOG_INFO,"принят сигнал SIGUSR1, ожидание завершения процесса");
    ShutdownFinishWork=1;
}


void Daemon::Close_Descriptors()
{
    if(PidFileDescriptor!=-1)
    {
        close(PidFileDescriptor);
        unlink(PidFilePath);
        PidFileDescriptor=-1;
    }
}