#ifndef CLEANER_CLEANER_H
#define CLEANER_CLEANER_H

#include <iostream>
#include <vector>
#include <models.h>
#include "daemon.h"


class Cleaner: public Daemon {
public:
    Cleaner();
    ~Cleaner()= default;

private:
    static time_t param_Sleep_time;
    static time_t param_Time;
    static std::string param_Mode;

    static time_t urlLifeTime;

    static bool Set_time(time_t time);
    static bool Set_sleep_time(time_t t);
    static bool Set_mode(const std::string& mode);

    static time_t Get_Time(const char *str_time);

    static int Clean();

	int Get_parameters(int argc, char** argv) override;
	int Daemon_work() override;

	static void Delete_expired_key();
};
#endif //CLEANER_CLEANER_H