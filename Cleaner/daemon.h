#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <unistd.h>
#include <fcntl.h>
#include <csignal>
#include <syslog.h>
#include <cerrno>
#include <sys/stat.h>

#ifndef CLIANER_DAEMON_H
#define CLIANER_DAEMON_H


class Daemon
{
public:
     Daemon()= default;
    ~Daemon() = default;

	void * Run(int argc, char** argv);

protected:
    static sig_atomic_t ShutdownFinishWork;
    static sig_atomic_t DaemonBusy;

private:
    static const char *PidFilePath;
    static int	PidFileDescriptor;
    static sig_atomic_t RestartWork;

    void Start();

    static int Become_daemon(const char *pidFileName,
                      const char *logPrefix, int logLevel, int *pidFileDescriptor);

    static int Signal_handling();

    static void Fatal_Sig_Handler(int sig);
    static void Term_Handler(int sig);
    static void Hup_Handler(int sig);
    static void Usr1_Handler(int sig);
    static void Close_Descriptors();

    virtual int Daemon_work() { return 0; };
    virtual int Get_parameters(int argc, char** argv) { return 0;};
};


#endif //CLIANER_DAEMON_H