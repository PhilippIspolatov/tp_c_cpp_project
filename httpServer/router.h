//
// Created by Филипп Исполатов on 2019-11-09.
//

#ifndef APPLICATIONSERVER_ROUTER_H
#define APPLICATIONSERVER_ROUTER_H

#include "./managers/keyManager.h"
#include "./managers/profileManager.h"
#include "serializers.h"
#include "./managers/cacheManager.h"



class Router {
public:
    Router(ProfileManager* profileManager, KeyManager* keyManager, CacheManager* cacheManager);
    Router() = default;
    virtual ~Router() = default;
    boost::beast::http::response<boost::beast::http::string_body> execute(boost::beast::http::request<boost::beast::http::string_body>& request);

private:
    ProfileManager* profileManager;
    KeyManager* keyManager;
    CacheManager* cacheManager;


};

#endif //APPLICATIONSERVER_ROUTER_H
