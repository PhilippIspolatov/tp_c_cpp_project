//
// Created by Филипп Исполатов on 28/11/2019.
//

#include "router.h"
#include <iostream>
#include <boost/property_tree/json_parser.hpp>

Router::Router(ProfileManager* profileManager, KeyManager* keyManager, CacheManager* cacheManager) :
    profileManager(profileManager),
    keyManager(keyManager),
    cacheManager(cacheManager) {}

boost::beast::http::response<boost::beast::http::string_body> Router::execute(
        boost::beast::http::request<boost::beast::http::string_body>& request) {

    boost::beast::http::response<boost::beast::http::string_body> response;
    response.result(boost::beast::http::status::not_found);

    switch (request.method()) {
        case boost::beast::http::verb::get : {
            if (request.target() == "/info") {
                response = keyManager->getInfo(request);
            }
            else
                response = keyManager->redirect(request);
            break;
        }
        case boost::beast::http::verb::post : {
            if (request.target() == "/registrate") {
                response = profileManager->regisrate(request);
            }
            else if (request.target() == "/generate") {
                response = keyManager->genKey(request);
            }
            else if (request.target() == "/login") {
                response = profileManager->login(request);
            }
            else if (request.target() == "/grant") {
                response = keyManager->grant(request);
            }
            break;
        }
        default :
            break;
    }
    return response;
}
