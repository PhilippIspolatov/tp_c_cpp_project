//
// Created by Филипп Исполатов on 2019-11-09.
//

#ifndef APPLICATIONSERVER_HTTPSERVER_H
#define APPLICATIONSERVER_HTTPSERVER_H

#include "router.h"
#include "models.h"

class HttpServer {
public:
    HttpServer(std::string& address, int port);

    void run();

protected:
    void accept();

    boost::asio::ip::tcp::endpoint endpoint;

    boost::asio::io_context context;

    boost::asio::ip::tcp::socket socket;

    boost::asio::ip::tcp::acceptor acceptor;

    UsersModel *usersModel = new UsersModel();
    UsedKeysModel* usedKeysModel = new UsedKeysModel();
    ProfileManager *profileManager = new ProfileManager(usersModel);
    KeyManager* keyManager = new KeyManager(usedKeysModel, usersModel);

    Router router = Router(profileManager, keyManager, nullptr);
};

#endif //APPLICATIONSERVER_HTTPSERVER_H
