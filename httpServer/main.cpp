//
// Created by Филипп Исполатов on 30/11/2019.
//
#include <iostream>
#include "httpServer.h"

int main(int argc, char* argv[]) {

    std::string address = "127.0.0.1";
    HttpServer *server = new HttpServer(address, 3000);
    server->run();

    return 0;
}