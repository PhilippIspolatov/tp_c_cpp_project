//
// Created by Филипп Исполатов on 2019-11-10.
//

#ifndef APPLICATIONSERVER_SERIALIZER_H
#define APPLICATIONSERVER_SERIALIZER_H

#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <istream>
#include <iostream>
#include <boost/beast.hpp>

struct GenerateKeyReq {
    std::string originalUrl;
    std::string alias;
    std::string username;
    std::string password;
    std::vector<std::string> grant;

    GenerateKeyReq(boost::beast::http::request<boost::beast::http::string_body>& request) {
        try {
            boost::property_tree::ptree tree;
            std::stringstream ss;
            ss << request.body();
            boost::property_tree::read_json(ss, tree);
            originalUrl = tree.get<std::string>("url");
            alias = tree.get<std::string>("alias");
        }
        catch(std::exception const& e) {
            std::cerr << e.what() << std::endl;
        }
    }
};

struct GrantReq {
    std::string hash;
    std::vector<std::string> grant;

    GrantReq(boost::beast::http::request<boost::beast::http::string_body>& request) {
        try {
            boost::property_tree::ptree tree;
            std::stringstream ss;
            ss << request.body();
            boost::property_tree::read_json(ss, tree);
            hash = tree.get<std::string>("simpleurl");
            for (boost::property_tree::ptree::value_type &el : tree.get_child("grant")) {
                grant.push_back(el.second.data());
            }
        }
        catch(std::exception const& e) {
            std::cerr << e.what() << std::endl;
        }
    }
};

struct RegistrateReq {
    std::string username;
    std::string password1;
    std::string password2;

    RegistrateReq(boost::beast::http::request<boost::beast::http::string_body>& request) {
        try {
            boost::property_tree::ptree tree;
            std::stringstream ss;
            ss << request.body();
            boost::property_tree::read_json(ss, tree);
            username = tree.get<std::string>("username");
            password1 = tree.get<std::string>("password1");
            password2 = tree.get<std::string>("password2");
        }
        catch(std::exception const& e) {
            std::cerr << e.what() << std::endl;
        }
    }
};

struct ProfileInfoReq {
    std::string username;
    std::string password;

    ProfileInfoReq(boost::beast::http::request<boost::beast::http::string_body>& request) {
        try {
            boost::property_tree::ptree tree;
            std::stringstream ss;
            ss << request.body();
            boost::property_tree::read_json(ss, tree);
            username = tree.get<std::string>("username");
            password = tree.get<std::string>("password");
        }
        catch(std::exception const& e) {
            std::cerr << e.what() << std::endl;
        }
    }
};



#endif //APPLICATIONSERVER_SERIALIZER_H
