//
// Created by Филипп Исполатов on 2019-11-09.
//

#ifndef APPLICATIONSERVER_HTTPSERVCONNECTION_H
#define APPLICATIONSERVER_HTTPSERVCONNECTION_H

#include "router.h"

using namespace boost::asio;

class HttpServerConnection : public boost::enable_shared_from_this<HttpServerConnection>{
public:
    HttpServerConnection(ip::tcp::socket socket, Router* router);
    ~HttpServerConnection() = default;

    void start();

protected:
    void doRead();

    void onRead();

    void doWrite();

    void onWrite();

    boost::beast::flat_buffer buffer{8192};

    boost::beast::http::request<boost::beast::http::string_body > request;

    boost::beast::http::response<boost::beast::http::string_body> response;

    ip::tcp::socket socket;

    Router* router;
};

#endif //APPLICATIONSERVER_HTTPSERVCONNECTION_H
