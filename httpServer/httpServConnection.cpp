//
// Created by Филипп Исполатов on 19/11/2019.
//

#include "httpServConnection.h"
#include <iostream>

HttpServerConnection::HttpServerConnection(ip::tcp::socket socket, Router *router) : socket(std::move(socket)),
    router(router) {}

void HttpServerConnection::start() {
    doRead();
}

void HttpServerConnection::doRead() {
    auto self = shared_from_this();

    boost::beast::http::async_read(socket, buffer, request, [self](boost::beast::error_code err, size_t bytes_transfered) {
        if(!err) {
            self->onRead();
        }
    });
}

void HttpServerConnection::onRead() {
    response = router->execute(request);
    std::cout << response << std::endl;
    doWrite();
}

void HttpServerConnection::doWrite() {
    auto self = shared_from_this();

    boost::beast::http::async_write(socket, response, [self](boost::beast::error_code err, size_t bytes_transfered) {
        if (!err) {
            self->socket.shutdown(ip::tcp::socket::shutdown_send, err);
        }
    });
}

void HttpServerConnection::onWrite() {

}