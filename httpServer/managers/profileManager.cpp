//
// Created by Филипп Исполатов on 30/11/2019.
//

#include "profileManager.h"
#include "../serializers.h"
#include "models.h"
#include <iostream>
#include "jwt/jwt.hpp"

ProfileManager::ProfileManager(UsersModel *users) : users(users) {}

boost::beast::http::response<boost::beast::http::string_body> ProfileManager::regisrate(
        boost::beast::http::request<boost::beast::http::string_body> request) {

    boost::beast::http::response<boost::beast::http::string_body> response;
    auto registrateJSON = RegistrateReq(request);

    if (passwordValidate(registrateJSON.password1, registrateJSON.password2)) {
        auto user = new User();
        user->username = registrateJSON.username;
        user->password = registrateJSON.password1;

        if (users->addUser(user)) {
            response.body() = "{ \"message\": \"success\" }";
            response.set(boost::beast::http::field::content_type, "application/json");
            response.result(boost::beast::http::status::ok);
        } else {
            response.body() = "{ \"err\": \"user with the same name already exists\" }";
            response.set(boost::beast::http::field::content_type, "application/json");
            response.result(boost::beast::http::status::bad_request);
        }
    }
    else {
        response.body() = "{ \"err\": \"different passwords\" }";
        response.set(boost::beast::http::field::content_type, "application/json");
        response.result(boost::beast::http::status::bad_request);
    }
    return response;

}

boost::beast::http::response<boost::beast::http::string_body> ProfileManager::login(
        boost::beast::http::request<boost::beast::http::string_body> request) {
    boost::beast::http::response<boost::beast::http::string_body> response;
    auto loginJSON = ProfileInfoReq(request);

    auto user = users->getUserByNickname(loginJSON.username);

    if (user.username.empty()) {
        response.body() = "{ \"err\": \"no such user\" }";
        response.result(boost::beast::http::status::bad_request);
        return response;
    }
    if (user.password != loginJSON.password) {
        response.body() = "{ \"err\": \"wrong password\" }";
        response.result(boost::beast::http::status::bad_request);
        return response;
    }

    jwt::jwt_object obj{jwt::params::algorithm("HS256"), jwt::params::payload({{"username", user.username}, {"password", user.password}}), jwt::params::secret("nikolassalokin")};
    auto enc_str = obj.signature();
    response.set(boost::beast::http::field::authorization, enc_str);
    response.result(boost::beast::http::status::ok);
    return response;
}

bool ProfileManager::passwordValidate(std::string &password1, std::string &password2) {
    return password1 == password2;
}

