//
// Created by Филипп Исполатов on 2019-11-10.
//

#ifndef APPLICATIONSERVER_KEYMANAGER_H
#define APPLICATIONSERVER_KEYMANAGER_H

#include "../serializers.h"

/* Обработка запросов связанных с ключами
 * обращается к БД (методы redirect и getInfo) и к keyService(genKey)
 */
#include <models.h>

#include <boost/beast.hpp>
class KeyManager {
public:
    KeyManager(UsedKeysModel *usedKeys, UsersModel* userModel);

    boost::beast::http::response<boost::beast::http::string_body> redirect(boost::beast::http::request<boost::beast::http::string_body> request);

    boost::beast::http::response<boost::beast::http::string_body> getInfo(boost::beast::http::request<boost::beast::http::string_body> request);

    boost::beast::http::response<boost::beast::http::string_body> genKey(boost::beast::http::request<boost::beast::http::string_body> request);

    boost::beast::http::response<boost::beast::http::string_body> grant(boost::beast::http::request<boost::beast::http::string_body> request);

    std::string userExist(boost::beast::http::request<boost::beast::http::string_body> &request);
private:
    //TcpClient *client;
    UsedKeysModel *usedKeys;
    UsersModel* userModel;
    //UserInfoSerializer *userInfoSerializer;
    
    bool checkRelevance(UsedKey *usedKey);	
};

#endif //APPLICATIONSERVER_KEYMANAGER_H