//
// Created by Филипп Исполатов on 30/11/2019.
//

#include "keyManager.h"
#include "../serializers.h"
#include "tcpClient.h"
#include "models.h"
#include <ostream>
#include "jwt/jwt.hpp"

KeyManager::KeyManager(UsedKeysModel *usedKeys, UsersModel *userModel) : usedKeys(usedKeys), userModel(userModel) {}

boost::beast::http::response<boost::beast::http::string_body>
KeyManager::genKey(boost::beast::http::request<boost::beast::http::string_body> request) {
    boost::beast::http::response<boost::beast::http::string_body> response;
    GenerateKeyReq reqStructure = GenerateKeyReq(request);

    auto username = userExist(request);
    if (username.empty()) {
        response.body() = "{ \"err\": \"bad params\" }";
        response.set(boost::beast::http::field::content_type, "application/json");
        response.result(boost::beast::http::status::bad_request);
        return response;
    }


    KeyQuery keyQuery;
    keyQuery.origUrl = reqStructure.originalUrl;
    keyQuery.alias = reqStructure.alias;
    keyQuery.username = username;

    std::stringstream query;
    msgpack::pack(query, keyQuery);
    std::cout << reqStructure.alias << std::endl;

    std::string client_response;
    std::string address = "127.0.0.1";
    std::string req = query.str();
    TcpClient client(address, 5000);

    client.request(req, client_response);
    std::cout << "Response: " << client_response << std::endl;
    msgpack::object_handle oh = msgpack::unpack(client_response.c_str(), client_response.size());
    msgpack::object deserialized = oh.get();

    auto result = deserialized.as<KeyResponse>();

    response.body() = result.hash;
    response.body() = "{ \"simpleurl\": \"" + result.hash + "\" }";
    response.set(boost::beast::http::field::content_type, "application/json");
    response.result(boost::beast::http::status::bad_request);

    return response;
}

boost::beast::http::response<boost::beast::http::string_body> KeyManager::redirect(
        boost::beast::http::request<boost::beast::http::string_body> request) {
    bool flag = false;
    boost::beast::http::response<boost::beast::http::string_body> response;
    std::string target = request.target().to_string();
    target.erase(target.begin());
    std::cout << target << std::endl;
    std::string key = target;

    auto permisionsModel =  new UserPerms();

    auto surl = usedKeys->getUsedKeyByAlias(target);
    if (!surl.key.empty()) {
        key = surl.key;
    }

    auto permisions = permisionsModel->getUserPermissions(key);
    if (permisions.hash.empty())
        flag = true;
    else {
        auto username = userExist(request);
        for (auto& el : permisions.users) {
            if (el == username) {
                flag = true;
            }
        }
    }

    if (!flag) {
        response.result(boost::beast::http::status::bad_request);
        return response;
    }

    auto usedKey = usedKeys->getUsedKeyByHash(target);
    if (usedKey.key.empty())
        usedKey = usedKeys->getUsedKeyByAlias(target);
    if (usedKey.key.empty()) {
        response.result(boost::beast::http::status::not_found);
    }
    if (!usedKey.key.empty()) {
        if (checkRelevance(&usedKey)){
            std::string url = usedKey.origUrl;
            response.result(boost::beast::http::status::moved_permanently);
            response.set(boost::beast::http::field::location, url);
        }
    else response.result(boost::beast::http::status::not_found);
    }
    return response;

}

boost::beast::http::response<boost::beast::http::string_body> KeyManager::getInfo(
        boost::beast::http::request<boost::beast::http::string_body> request) {
    boost::beast::http::response<boost::beast::http::string_body> response;

    auto username = userExist(request);
    if (username.empty()) {
        response.body() = "{ \"err\": \"bad params\" }";
        response.set(boost::beast::http::field::content_type, "application/json");
        response.result(boost::beast::http::status::bad_request);
        return response;
    }

    auto info = usedKeys->getUsedKeysByNickname(username);
    response.body() = "{\n";
    for (auto& item : info) {
        response.body() += "\t{\n";
        response.body() += "\t\t\"url\": \"" + item.origUrl + "\", \n";
        response.body() += "\t\t\"simpleurl\": \"" + item.key + "\", \n";
        response.body() += "\t\t\"alias\": \"" + item.alias + "\" \n";
        response.body() += "\t},\n";
    }
    response.body() += "}";

    response.set(boost::beast::http::field::content_type, "application/json");
    response.result(boost::beast::http::status::ok);
    return response;
}

boost::beast::http::response<boost::beast::http::string_body> KeyManager::grant(
        boost::beast::http::request<boost::beast::http::string_body> request) {
    boost::beast::http::response<boost::beast::http::string_body> response;

    auto username = userExist(request);

    if (username.empty()) {
        response.body() = "{ \"err\": \"no user\" }";
        response.set(boost::beast::http::field::content_type, "application/json");
        response.result(boost::beast::http::status::bad_request);
        return response;
    }

    auto grantReq = GrantReq(request);

    std::cout << grantReq.hash << std::endl;

    auto access = usedKeys->getUsedKeyByHash(grantReq.hash);
    if (username != access.username) {
        response.body() = "{ \"err\": \"wrong password\" }";
        response.set(boost::beast::http::field::content_type, "application/json");
        response.result(boost::beast::http::status::bad_request);
        return response;
    }

    auto permisionsModel =  new UserPerms();
    auto perm = permisionsModel->getUserPermissions(grantReq.hash);
    if (perm.hash.empty()) {
        permisionsModel->addOwner(grantReq.hash, username);
    }
    if (permisionsModel->addPermission(grantReq.hash, grantReq.grant)) {
        response.result(boost::beast::http::status::ok);
        return response;
    }
    else {
        response.body() = "{ \"err\": \"internal error\" }";
        response.set(boost::beast::http::field::content_type, "application/json");
        response.result(boost::beast::http::status::bad_request);
        return response;
    }

}

std::string KeyManager::userExist(boost::beast::http::request<boost::beast::http::string_body> &request) {

    std::string authorization = request["Authorization"].to_string();

    std::error_code ec;

    auto dec_obj = jwt::decode(authorization, jwt::params::algorithms({"hs256"}), ec, jwt::params::secret("nikolassalokin"));

    if (ec) {
        return "";
    }
    boost::property_tree::ptree tree;
    std::stringstream ss;
    ss << dec_obj.payload();

    boost::property_tree::read_json(ss, tree);
    auto username = tree.get<std::string>("username");
    auto password = tree.get<std::string>("password");

    auto user = userModel->getUserByNickname(username);

    if (!user.username.empty()) {
        if (user.password == password)
            return user.username;
    }
    return "";
}

bool KeyManager::checkRelevance(UsedKey *usedKey) {
    unsigned int urlLifeTime = 3600*24;
    if (usedKey->expiration+urlLifeTime < std::time(nullptr)) {
        usedKeys->deleteUsedKey(usedKey->key);
        return false;
    }
    return true;
}
