//
// Created by Филипп Исполатов on 2019-11-10.
//

#ifndef APPLICATIONSERVER_PROFILEMANAGER_H
#define APPLICATIONSERVER_PROFILEMANAGER_H

#include "../serializers.h"
#include <models.h>

class ProfileManager {
public:
    ProfileManager(UsersModel * users);

    boost::beast::http::response<boost::beast::http::string_body> regisrate(boost::beast::http::request<boost::beast::http::string_body> request);
    boost::beast::http::response<boost::beast::http::string_body> login(boost::beast::http::request<boost::beast::http::string_body> request);
private:
    bool passwordValidate(std::string& password1, std::string& password2);
    UsersModel * users;
    //UserInfoSerializer* serializer;
};

#endif //APPLICATIONSERVER_PROFILEMANAGER_H