#ifndef KEY_KEYMANAGER_H
#define KEY_KEYMANAGER_H

#include <models.h>
#include <ctime>
#include <map>
#include <vector>
#include "KeyGenerator.h"

#include <msgpack.hpp>

class KeyManager {
public:
    KeyManager();
    KeyManager(UsedKeysModel *uKeys, KeyGenerator *generator) : uKeys(uKeys), generator(generator) {}

    std::string generateKey(KeyQuery query);

private:
    UsedKeysModel *uKeys;
    KeyGenerator *generator;
};


#endif //KEY_KEYMANAGER_H
