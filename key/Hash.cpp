#include "Hash.h"

std::string Hash::hash() {
    std::string res;
    for (size_t i = 0; i < resultSize; i++)
        res += alphabet[std::rand() % ALPHABET_SIZE];

    return res;
}

std::vector<std::string> Hash::getHashArray(const size_t count) {
    std::vector<std::string> vector;
    for (size_t i = 0; i < count; i++) {
        std::string key = hash();
        if (key.size())
            vector.push_back(key);
    }
    return vector;
}
