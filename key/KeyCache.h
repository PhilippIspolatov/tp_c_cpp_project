#ifndef TP_C_CPP_PROJECT_KEYCACHE_H
#define TP_C_CPP_PROJECT_KEYCACHE_H
#include <unordered_set>
#include <vector>
#include "Hash.h"

#define PRE_LOAD_SIZE 1000
#define MAX_BALANCE_SIZE 200

class KeyCache {
public:
    KeyCache();
    KeyCache(std::unordered_set<std::string> *main, std::unordered_set<std::string> *aux) : mainCache(main), auxiliaryCache(aux) {};

    std::string getKey();
    int pushNewKeys(std::vector<std::string> newKeys);
    bool find(std::string key);

    size_t getMainSize();
    size_t getAuxSize();

    size_t pushNewKeysIntoMain(std::vector<std::string> newKeys);
    size_t pushNewKeysIntoAux(std::vector<std::string> newKeys);

    void swapStorages();

private:
    std::unordered_set<std::string> *mainCache;
    std::unordered_set<std::string> *auxiliaryCache;

};


#endif //TP_C_CPP_PROJECT_KEYCACHE_H
