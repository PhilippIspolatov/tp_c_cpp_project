//
// Created by Nikolas on 10/11/2019.
//

#include "KeyManager.h"

std::string KeyManager::generateKey(KeyQuery query) {
    std::string newKey = generator->getNewKey();

    if (!newKey.empty()) {
        UsedKey newTuple;

        newTuple.key = newKey;
        newTuple.username = query.username;
        newTuple.alias = query.alias;
        newTuple.origUrl = query.origUrl;
        newTuple.expiration = std::time(nullptr);
        bool result = uKeys->addUsedKey(newTuple);

        return newKey;
    }

    return std::string();
}

KeyManager::KeyManager() {
    uKeys = new UsedKeysModel();
    generator = new KeyGenerator();
}
