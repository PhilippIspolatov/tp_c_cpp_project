//
// Created by Nikolas on 10/11/2019.
//

#ifndef KEY_KEYGENERATOR_H
#define KEY_KEYGENERATOR_H

#include <vector>
#include <thread>
#include "KeyCache.h"
#include "Hash.h"
#include "models.h"

#define PRELOAD_SIZE 1000


class KeyGenerator {
public:
    KeyGenerator();
    KeyGenerator(UsedKeysModel *uKeys, Hash *hash, KeyCache *cache) : uKeys(uKeys), hash(hash), cache(cache) {}

    std::string getNewKey();

    void preLoadStorage();

    void validateNewKeys(std::vector<std::string> newKeys);

    void startMonitorStorage();

    void stopMonitorStorage();

private:
    std::vector<std::string> genKeyArray(size_t count);

    void monitor(KeyCache &cache);

    UsedKeysModel *uKeys;
    Hash *hash;
    KeyCache *cache;

    std::shared_ptr<std::thread> thread_ptr;
    bool statusMonitoring;
};


#endif //KEY_KEYGENERATOR_H
