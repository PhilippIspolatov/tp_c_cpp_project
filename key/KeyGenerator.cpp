//
// Created by Nikolas on 10/11/2019.
//

#include "KeyGenerator.h"
#include <memory>

std::string KeyGenerator::getNewKey() {
    return cache->getKey();
}

std::vector<std::string> KeyGenerator::genKeyArray(size_t count) {
    std::vector<std::string> vector;
    std::string key;
    size_t cur_size = vector.size();

    while (vector.size() != count) {
        for (size_t i = 0; i < count - cur_size; i++) {
            key = hash->hash();
            vector.push_back(key);
        }

//        vector = uKeys->validateKeys(vector);
        cur_size = vector.size();
    }

    return vector;
}

void KeyGenerator::validateNewKeys(std::vector<std::string> newKeys) {
    for (auto key : newKeys) {
        if (cache->find(key)) {
            newKeys.push_back(key);
        }
    }
}

void KeyGenerator::preLoadStorage() {
    auto main = genKeyArray(PRELOAD_SIZE);
    auto aux = genKeyArray(PRELOAD_SIZE);

    validateNewKeys(aux);

    cache->pushNewKeysIntoMain(main);
    cache->pushNewKeysIntoAux(aux);

    std::cout << cache->getMainSize() << cache->getAuxSize() << std::endl;
}

void KeyGenerator::startMonitorStorage() {
    statusMonitoring = true;
    thread_ptr = std::make_shared<std::thread>(std::thread([&]() {
        this->monitor(*cache);
    }));
}

void KeyGenerator::stopMonitorStorage() {
    statusMonitoring = false;
    thread_ptr->join();
}

void KeyGenerator::monitor(KeyCache &cache){

    while (statusMonitoring) {
        if (cache.getAuxSize() < MAX_BALANCE_SIZE) {
            auto vector = genKeyArray(PRE_LOAD_SIZE);
        }
    }

    std::cout << "terminate" << std::endl;
}

KeyGenerator::KeyGenerator() {
    uKeys = new UsedKeysModel();
    hash = new Hash(7);
    cache = new KeyCache();

    preLoadStorage();
}
