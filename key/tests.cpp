#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "KeyManager.h"
#include "database.h"

using ::testing::AtLeast;
using ::testing::DoAll;
using ::testing::Return;
using ::testing::SetArgReferee;

class MockDBuKeysClient : public UsedKeysModel {
public:
    MOCK_METHOD(UsedKey *, getUsedKeyByHash, (std::string hash));
    MOCK_METHOD(UsedKey *, getUsedKeyByAlias, (std::string alias));
    MOCK_METHOD(int, addUsedKey, (UsedKey *uKey));
};

class MockKeyGenerator : public KeyGenerator {
public:
    MockKeyGenerator() : KeyGenerator(nullptr, nullptr, nullptr) {}
    MOCK_METHOD(std::string, getNewKey, ());
    MOCK_METHOD(std::vector<std::string>, genKeyArray, (size_t count));
};

class MockKeyCache : public KeyCache {
public:
    MOCK_METHOD(std::string, getKey, ());
    MOCK_METHOD(size_t, getStorageCapacity, ());
};

class MockHash : public Hash {
public:
    MOCK_METHOD(std::string, hash, ());
};

TEST(KeyService, genKey) {
    MockDBuKeysClient client;
    UsedKey *key = nullptr;
    EXPECT_CALL(client, addUsedKey(key)).Times(AtLeast(1));

    KeyManager manager(&client, nullptr);
    std::map<std::string, std::string> map;
    manager.generateKey(map);
}

TEST(KeyService, generatorGet) {
    MockKeyGenerator generator;
    EXPECT_CALL(generator, getNewKey()).Times(AtLeast(1));

    KeyManager manager(nullptr, &generator);
    std::map<std::string, std::string> map;
    manager.generateKey(map);
}

TEST(KeyService, cacheGet) {
    MockKeyCache cache;
    EXPECT_CALL(cache, getKey()).Times(AtLeast(1));

    KeyGenerator generator(nullptr, nullptr, &cache);
    generator.getNewKey();
}

TEST(KeyService, cacheStorageCheck) {
    MockKeyCache cache;
    EXPECT_CALL(cache, getStorageCapacity()).Times(AtLeast(1));

    KeyGenerator generator(nullptr, nullptr, &cache);
    generator.getNewKey();
}

TEST(KeyService, cachePushStorage) {
    MockKeyCache cache;
    EXPECT_CALL(cache, getStorageCapacity()).Times(AtLeast(1));

    KeyGenerator generator(nullptr, nullptr, &cache);
    generator.getNewKey();
}

TEST(KeyService, genKeyArray) {
    MockHash hash;
    EXPECT_CALL(hash, hash()).Times(AtLeast(1));

    KeyGenerator generator(nullptr, &hash, nullptr);
    generator.genKeyArray(1000);
}


int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}