#include "KeyCache.h"

KeyCache::KeyCache() {
    auxiliaryCache = new std::unordered_set<std::string>();
    mainCache = new std::unordered_set<std::string>;
}

std::string KeyCache::getKey() {
    std::string key = mainCache->extract(mainCache->begin()).value();
    if (mainCache->size() == MAX_BALANCE_SIZE) {
        swapStorages();
    }

    return key;
}

bool KeyCache::find(std::string key) {
    return (mainCache->find(key) != mainCache->end()) && (auxiliaryCache->find(key) != auxiliaryCache->end());
}

size_t KeyCache::pushNewKeysIntoMain(std::vector<std::string> newKeys) {
    for (const auto& key : newKeys) {
        mainCache->insert(key);
    }
    return mainCache->size();
}

size_t KeyCache::pushNewKeysIntoAux(std::vector<std::string> newKeys) {
    for (const auto& key : newKeys) {
        auxiliaryCache->insert(key);
    }
    return auxiliaryCache->size();
}

int KeyCache::pushNewKeys(std::vector<std::string> newKeys) {
    return 0;
}

size_t KeyCache::getMainSize() {
    return mainCache->size();
}

size_t KeyCache::getAuxSize() {
    return auxiliaryCache->size();
}

void KeyCache::swapStorages() {
    auto temp = mainCache;
    mainCache = auxiliaryCache;
    auxiliaryCache = temp;
}
