#ifndef KEY_HASH_H
#define KEY_HASH_H

#include <cstdlib>
#include <iostream>
#include <ctime>
#include <vector>

#define ALPHABET_SIZE 26

class Hash {
public:
    Hash(size_t size) : resultSize(size) {
        std::srand(unsigned(std::time(0)));
    }
    std::string hash();
    std::vector<std::string> getHashArray(const size_t count);

private:
    size_t resultSize;
    char alphabet[ALPHABET_SIZE] = {'a', 'b', 'c', 'd', 'e', 'f', 'g',
                                    'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                    'o', 'p', 'q', 'r', 's', 't', 'u',
                                    'v', 'w', 'x', 'y', 'z'};
};


#endif //KEY_HASH_H
