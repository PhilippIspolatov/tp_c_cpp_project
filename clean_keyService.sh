cd ./database
echo "1-------------"
ls
rm -rf cmake-build-debug
echo "Database build was deleted ..."

cd ../key
echo "2-------------"
ls
rm -rf cmake-build-debug
echo "Key build was deleted ..."

cd ../tcpServer
echo "3-------------"
ls
rm -rf cmake-build-debug
echo "TCP Server build was deleted ..."

echo "Finish"