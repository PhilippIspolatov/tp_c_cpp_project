cd ./database
pwd
mkdir cmake-build-debug
cd ./cmake-build-debug
cmake ..
make
cd ./tarantool-c-build
make
cd ./tnt
make
cd ../..
echo "Database lib was build ..."

cd ../../key
mkdir cmake-build-debug
cd ./cmake-build-debug
cmake ..
make
echo "Key lib was build ..."

cd ../../tcpServer
mkdir cmake-build-debug
cd ./cmake-build-debug
cmake ..
make
echo "Server was build ..."

echo "Run -> "
./tcpServer